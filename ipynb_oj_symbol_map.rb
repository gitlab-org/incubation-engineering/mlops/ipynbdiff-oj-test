module IpynbDiff
  require 'oj'

  class IpynbSajSymbolMap
    class << self
      def parse(file_name)
        parser = IpynbDiff::IpynbSajSymbolMap.new
        Oj::Parser.saj.handler = parser
        Oj::Parser.saj.parse(File.read(file_name))

        parser.symbols.to_h { |x| [".#{x[:symbol]}", x[:line]] }
      end
    end

    attr_accessor :symbols

    def initialize
      super

      @current_path = []
      @current_path_line_starts = []
      @symbols = []
      @current_array_index = []
    end

    def hash_start(key, line, column)
      if key.nil? || key.empty?
        symbol = @current_array_index.pop || 0
        @current_array_index << symbol + 1
      else
        symbol = key
      end

      @symbols << { symbol: @current_path.append(symbol).join('.'), line: line } unless symbols.empty? && symbol.zero?
    end

    def hash_end(key, line, column)
      @current_path.pop
    end

    def array_start(key, line, column)
      @current_path << key
      @current_array_index << 0

      @symbols << { symbol: @current_path.join('.'), line: line }
    end

    def array_end(key, line, column)
      @current_path.pop
      @current_array_index.pop
    end

    def add_value(value, key, line, column)
      if key.nil? || key.empty?
        index = @current_array_index.pop
        symbol = @current_path.append(index).join('.')
        @current_array_index << index + 1
      else
        symbol = @current_path.append(key).join('.')
      end

      @symbols << {symbol: symbol, line: line}

      @current_path.pop
    end
  end
end