Reimplements ipynbdiff json symbol maps with OJ

- Own modifications
```
Benchmarking notebooks/input.json

Rehearsal ----------------------------------------------------
modified_oj        0.000047   0.000036   0.000083 (  0.000079)
modified_oj_2      0.000128   0.000075   0.000203 (  0.000174)
ipynb_symbol_map   0.000185   0.000033   0.000218 (  0.000218)
------------------------------------------- total: 0.000301sec

                       user     system      total        real
modified_oj        0.000051   0.000049   0.000100 (  0.000099)
modified_oj_2      0.000054   0.000041   0.000095 (  0.000093)
ipynb_symbol_map   0.000166   0.000067   0.000233 (  0.000230)
Calculating -------------------------------------
         modified_oj     8.598k memsize (     0.000  retained)
                       128.000  objects (     0.000  retained)
                        47.000  strings (     0.000  retained)
         modified_oj_2     9.418k memsize (     4.038k retained)
                       116.000  objects (    33.000  retained)
                        41.000  strings (    14.000  retained)
    ipynb_symbol_map    17.068k memsize (     0.000  retained)
                       363.000  objects (     0.000  retained)
                        50.000  strings (     0.000  retained)
                        
Benchmarking notebooks/large_notebook.json

Rehearsal ----------------------------------------------------
modified_oj        0.000524   0.000168   0.000692 (  0.000690)
modified_oj_2      0.000498   0.000224   0.000722 (  0.000719)
ipynb_symbol_map   0.062768   0.003166   0.065934 (  0.066317)
------------------------------------------- total: 0.066626sec

                       user     system      total        real
modified_oj        0.000543   0.000137   0.000680 (  0.000676)
modified_oj_2      0.000487   0.000160   0.000647 (  0.000646)
ipynb_symbol_map   0.049941   0.002161   0.052102 (  0.052310)
Calculating -------------------------------------
         modified_oj   295.930k memsize (     0.000  retained)
                         1.511k objects (     0.000  retained)
                        50.000  strings (     0.000  retained)
       modified_oj_2   487.907k memsize (    47.745k retained)
                         1.410k objects (   358.000  retained)
                        50.000  strings (    50.000  retained)
    ipynb_symbol_map     8.040M memsize (     0.000  retained)
                       195.820k objects (     0.000  retained)
                        50.000  strings (     0.000  retained)
```
