require 'oj'
require_relative 'ipynb_symbol_map'
require_relative 'ipynb_oj_symbol_map'
require 'benchmark'
require 'benchmark/memory'

def run_oj(file_name)
  IpynbDiff::IpynbSajSymbolMap.parse(file_name)
end

puts(run_oj('notebooks/input.json'))


def run_symbol_map(file_name)
  IpynbDiff::IpynbSymbolMap.parse(File.read(file_name))
end

puts(run_symbol_map('notebooks/input.json'))

def cases(benchmark_runner, file_name)
  benchmark_runner.report('modified_oj_2') { run_oj(file_name) }
  benchmark_runner.report('ipynb_symbol_map') { run_symbol_map(file_name) }
end

def run_benchmark(file_name)
  puts("Benchmarking #{file_name}\n\n")

  Benchmark.bmbm { |x| cases(x, file_name) }
  Benchmark.memory { |x| cases(x, file_name) }
end

run_benchmark('notebooks/input.json')
run_benchmark('notebooks/large_notebook.json')
